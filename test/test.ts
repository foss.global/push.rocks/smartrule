import { expect, tap } from '@pushrocks/tapbundle';
import * as smartrule from '../ts/index.js';

interface ITestMessage {
  id: string;
  body: string;
}

let testSmartruleInstance: smartrule.SmartRule<ITestMessage>;

tap.test('first test', async () => {
  testSmartruleInstance = new smartrule.SmartRule<ITestMessage>();
  testSmartruleInstance.createRule(
    2,
    async (messageArg) => {
      if (messageArg.body.startsWith('hello')) {
        return 'apply-stop';
      }
    },
    async (messageArg) => {
      console.log(`rule triggered for message with body ${messageArg.body}`);
    }
  );
});

tap.test('make a decision based on an object', async () => {
  testSmartruleInstance.makeDecision({
    id: '123456',
    body: 'hello, there. This is a cool message!',
  });
});

tap.start();
