/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartrule',
  version: '2.0.1',
  description: 'a smart rule library for handling decision trees.'
}
