import * as plugins from './smartrule.plugins.js';
import { SmartRule } from './smartrule.classes.smartrule.js';

export type TTreeActionResult = 'continue' | 'apply-continue' | 'apply-stop' | 'stop';
export type TActionFunc<T = any> = (objectArg: T) => Promise<any>;

export type TCheckFunc<T> = (objectArg: T) => Promise<TTreeActionResult>;

export class Rule<T> {
  public smartRuleRef: SmartRule<T>;
  public priority: number;
  public checkFunction: TCheckFunc<T>;
  public actionFunction: TActionFunc;

  constructor(
    smartRuleRef: SmartRule<T>,
    priorityArg: number,
    checkFunctionArg: TCheckFunc<T>,
    actionFunctionArg: TActionFunc
  ) {
    this.smartRuleRef = smartRuleRef;
    this.priority = priorityArg;
    this.checkFunction = checkFunctionArg;
    this.actionFunction = actionFunctionArg;
  }
}
