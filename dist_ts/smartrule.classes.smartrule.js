import './smartrule.plugins.js';
import { Rule } from './smartrule.classes.rule.js';
export class SmartRule {
    constructor() {
        this.rules = [];
    }
    /**
     * makes a decision based on the given obect and the given rules
     * @param objectArg
     */
    async makeDecision(objectArg) {
        // lets sort the rules
        this.rules = this.rules.sort((a, b) => {
            if (a.priority > b.priority) {
                return 1;
            }
            else {
                return 0;
            }
        });
        // gets the next batch with the same priority
        const getNextParallelBatch = (priorityStart) => {
            return this.rules.filter(rule => {
                return rule.priority === priorityStart;
            });
        };
        // lets run the checks
        const runNextBatch = async (startPriority, runRulesAmount) => {
            const nextBatch = getNextParallelBatch(startPriority);
            runRulesAmount = runRulesAmount + nextBatch.length;
            const outcomes = [];
            for (const rule of nextBatch) {
                const checkResult = await rule.checkFunction(objectArg);
                checkResult
                    ? null
                    : console.log('WARNING!!! Please make sure your rule always returns a statement of how to continue!');
                if (checkResult.startsWith('apply')) {
                    await rule.actionFunction(objectArg); // here the action function is run
                }
                outcomes.push(checkResult);
            }
            if (outcomes.length > 0) {
                const finalOutcomeOfBatch = outcomes.reduce((previous, current, index, array) => {
                    if (current.includes('continue') || previous.includes('continue')) {
                        return 'continue';
                    }
                    else {
                        return 'stop';
                    }
                });
                if (finalOutcomeOfBatch === 'stop') {
                    return;
                }
            }
            if (runRulesAmount < this.rules.length) {
                await runNextBatch(startPriority + 1, runRulesAmount);
            }
            else {
                return;
            }
        };
        await runNextBatch(0, 0);
    }
    createRule(priorityArg, checkFunctionArg, actionFunctionArg) {
        const rule = new Rule(this, priorityArg, checkFunctionArg, actionFunctionArg);
        this.rules.push(rule);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic21hcnRydWxlLmNsYXNzZXMuc21hcnRydWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vdHMvc21hcnRydWxlLmNsYXNzZXMuc21hcnRydWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQXlCLHdCQUF3QixDQUFDO0FBQ2xELE9BQU8sRUFBRSxJQUFJLEVBQThDLE1BQU0sNkJBQTZCLENBQUM7QUFFL0YsTUFBTSxPQUFPLFNBQVM7SUFBdEI7UUFDUyxVQUFLLEdBQW1CLEVBQUUsQ0FBQztJQTJFcEMsQ0FBQztJQXpFQzs7O09BR0c7SUFDSSxLQUFLLENBQUMsWUFBWSxDQUFDLFNBQVk7UUFDcEMsc0JBQXNCO1FBQ3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDcEMsSUFBSSxDQUFDLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUU7Z0JBQzNCLE9BQU8sQ0FBQyxDQUFDO2FBQ1Y7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLENBQUM7YUFDVjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUgsNkNBQTZDO1FBQzdDLE1BQU0sb0JBQW9CLEdBQUcsQ0FBQyxhQUFxQixFQUFFLEVBQUU7WUFDckQsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDOUIsT0FBTyxJQUFJLENBQUMsUUFBUSxLQUFLLGFBQWEsQ0FBQztZQUN6QyxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVGLHNCQUFzQjtRQUN0QixNQUFNLFlBQVksR0FBRyxLQUFLLEVBQUUsYUFBcUIsRUFBRSxjQUFzQixFQUFpQixFQUFFO1lBQzFGLE1BQU0sU0FBUyxHQUFHLG9CQUFvQixDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3RELGNBQWMsR0FBRyxjQUFjLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQztZQUNuRCxNQUFNLFFBQVEsR0FBd0IsRUFBRSxDQUFDO1lBQ3pDLEtBQUssTUFBTSxJQUFJLElBQUksU0FBUyxFQUFFO2dCQUM1QixNQUFNLFdBQVcsR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3hELFdBQVc7b0JBQ1QsQ0FBQyxDQUFDLElBQUk7b0JBQ04sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQ1Qsc0ZBQXNGLENBQ3ZGLENBQUM7Z0JBRU4sSUFBSSxXQUFXLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNuQyxNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxrQ0FBa0M7aUJBQ3pFO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDNUI7WUFFRCxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN2QixNQUFNLG1CQUFtQixHQUFzQixRQUFRLENBQUMsTUFBTSxDQUM1RCxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUNsQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLElBQUksUUFBUSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsRUFBRTt3QkFDakUsT0FBTyxVQUFVLENBQUM7cUJBQ25CO3lCQUFNO3dCQUNMLE9BQU8sTUFBTSxDQUFDO3FCQUNmO2dCQUNILENBQUMsQ0FDRixDQUFDO2dCQUNGLElBQUksbUJBQW1CLEtBQUssTUFBTSxFQUFFO29CQUNsQyxPQUFPO2lCQUNSO2FBQ0Y7WUFFRCxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTtnQkFDdEMsTUFBTSxZQUFZLENBQUMsYUFBYSxHQUFHLENBQUMsRUFBRSxjQUFjLENBQUMsQ0FBQzthQUN2RDtpQkFBTTtnQkFDTCxPQUFPO2FBQ1I7UUFDSCxDQUFDLENBQUM7UUFFRixNQUFNLFlBQVksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUVNLFVBQVUsQ0FDZixXQUFtQixFQUNuQixnQkFBK0IsRUFDL0IsaUJBQThCO1FBRTlCLE1BQU0sSUFBSSxHQUFHLElBQUksSUFBSSxDQUFJLElBQUksRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUNqRixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4QixDQUFDO0NBQ0YifQ==