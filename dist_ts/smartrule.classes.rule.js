import './smartrule.plugins.js';
import './smartrule.classes.smartrule.js';
export class Rule {
    constructor(smartRuleRef, priorityArg, checkFunctionArg, actionFunctionArg) {
        this.smartRuleRef = smartRuleRef;
        this.priority = priorityArg;
        this.checkFunction = checkFunctionArg;
        this.actionFunction = actionFunctionArg;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic21hcnRydWxlLmNsYXNzZXMucnVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3RzL3NtYXJ0cnVsZS5jbGFzc2VzLnJ1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBeUIsd0JBQXdCLENBQUM7QUFDbEQsT0FBMEIsa0NBQWtDLENBQUM7QUFPN0QsTUFBTSxPQUFPLElBQUk7SUFNZixZQUNFLFlBQTBCLEVBQzFCLFdBQW1CLEVBQ25CLGdCQUErQixFQUMvQixpQkFBOEI7UUFFOUIsSUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUM7UUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxXQUFXLENBQUM7UUFDNUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxnQkFBZ0IsQ0FBQztRQUN0QyxJQUFJLENBQUMsY0FBYyxHQUFHLGlCQUFpQixDQUFDO0lBQzFDLENBQUM7Q0FDRiJ9