import { SmartRule } from './smartrule.classes.smartrule.js';
export declare type TTreeActionResult = 'continue' | 'apply-continue' | 'apply-stop' | 'stop';
export declare type TActionFunc<T = any> = (objectArg: T) => Promise<any>;
export declare type TCheckFunc<T> = (objectArg: T) => Promise<TTreeActionResult>;
export declare class Rule<T> {
    smartRuleRef: SmartRule<T>;
    priority: number;
    checkFunction: TCheckFunc<T>;
    actionFunction: TActionFunc;
    constructor(smartRuleRef: SmartRule<T>, priorityArg: number, checkFunctionArg: TCheckFunc<T>, actionFunctionArg: TActionFunc);
}
