import { Rule, TCheckFunc, TActionFunc } from './smartrule.classes.rule.js';
export declare class SmartRule<T> {
    rules: Array<Rule<T>>;
    /**
     * makes a decision based on the given obect and the given rules
     * @param objectArg
     */
    makeDecision(objectArg: T): Promise<void>;
    createRule(priorityArg: number, checkFunctionArg: TCheckFunc<T>, actionFunctionArg: TActionFunc): void;
}
